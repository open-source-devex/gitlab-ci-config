stages:
  - validate
  - plan-dev
  - plan-tst-acc-prd
  - apply-dev
  - apply-tst
  - apply-acc
  - apply-prd
  - promote


variables:
  TERRAFORM_PLAN_FILE: "${ENVIRONMENT}-terraform.tfplan"


default:
  image: registry.gitlab.com/open-source-devex/containers/build-terraform:latest


build:
  stage: validate
  script:
    - make init validate
  artifacts:
    paths:
      - .terraform
    expire_in: 1 month
  except:
    - tags


# #########################################
# Plan job instances
# #########################################
.plan:
  script:
    - |
      if [[ ! -d .terraform  ]]; then

          echo "==> Initialize terraform before making plan"
          make init

      fi

      make plan
  artifacts:
    paths:
      - .terraform
      - ${TERRAFORM_PLAN_FILE}
    expire_in: 1 month
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /^Incremented version.*/


dev:plan:
  extends: .plan
  dependencies:
    - build
  stage: plan-dev
  variables:
    ENVIRONMENT: dev
  only:
    - master


tst:plan:
  extends: .plan
  stage: plan-tst-acc-prd
  variables:
    ENVIRONMENT: tst
    # Used by Makefile
    init_flags: "-get=true -get-plugins=true"
  only:
    - tags

acc:plan:
  extends: .plan
  stage: plan-tst-acc-prd
  variables:
    ENVIRONMENT: acc
    # Used by Makefile
    init_flags: "-get=true -get-plugins=true"
  only:
    - tags


prd:plan:
  extends: .plan
  stage: plan-tst-acc-prd
  variables:
    ENVIRONMENT: prd
    # Used by Makefile
    init_flags: "-get=true -get-plugins=true"
  only:
    - tags


# #########################################
# Apply job instances
# #########################################
.apply:
  script:
    - |
      if [[ -f "${TERRAFORM_PLAN_FILE}" && -z "${CI_FORCE_REPLAN_BEFORE_APPLY}" ]]; then

          echo "==> Apply existing plan"
          make apply

      else

          echo "==> Initialize terraform and apply new plan"
          make all

      fi
  environment:
    name: ${ENVIRONMENT}
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /^Incremented version.*/


dev:apply:
  extends: .apply
  stage: apply-dev
  dependencies:
    - dev:plan
  variables:
    ENVIRONMENT: dev
  only:
    - master


tst:apply:
  extends: .apply
  stage: apply-tst
  dependencies:
    - tst:plan
  variables:
    ENVIRONMENT: tst
  when: manual
  allow_failure: false
  only:
    - tags

acc:apply:
  extends: .apply
  stage: apply-acc
  dependencies:
    - tst:apply
    - acc:plan
  variables:
    ENVIRONMENT: acc
  when: manual
  allow_failure: false
  only:
    - tags


prd:apply:
  extends: .apply
  stage: apply-prd
  dependencies:
    - acc:apply
    - prd:plan
  variables:
    ENVIRONMENT: prd
  when: manual
  allow_failure: false
  only:
    - tags

include:
  - project: 'open-source-devex/gitlab-ci-config'
    file: '/job-release.yaml'
